<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\PengajuanJaminanController;
use App\Http\Controllers\ApprovalController;
use App\Http\Controllers\DataJaminanController;
use App\Http\Controllers\SuretyBondController;
use App\Http\Controllers\MenejemenUserController;
use App\Http\Controllers\SettingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'admin/login');

Route::prefix('admin')->group(function() {
    Auth::routes();
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('/dashboard', DashboardController::class);
    Route::resource('/laporan', LaporanController::class);
    Route::resource('/pengajuan_jaminan', PengajuanJaminanController::class);
    Route::resource('/approval', ApprovalController::class);
    Route::resource('/data_jaminan', DataJaminanController::class);
    Route::resource('/data_surety_bond', SuretyBondController::class);
    Route::resource('/menejemen_user', MenejemenUserController::class);
    Route::resource('/setting', SettingController::class);

});
