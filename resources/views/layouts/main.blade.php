<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Test Project | {{ $title }}</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    @yield('style')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </nav>

        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <a href="../../index3.html" class="brand-link">
                <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">AdminLTE 3</span>
            </a>

            <div class="sidebar">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                    </div>
                </div>

                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        @if (Auth::user()->role->role_name == 'Administrator')
                            <li class="nav-item">
                                <a href="{{ route('dashboard.index') }}"
                                    class="nav-link {{ $title === 'Dashboard' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('laporan.index') }}"
                                    class="nav-link {{ $title === 'Laporan' ? 'active' : '' }}">
                                    <i class="nav-icon far fa-file-alt"></i>
                                    <p>Laporan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('pengajuan_jaminan.index') }}"
                                    class="nav-link {{ $title === 'Pengajuan Jaminan' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tasks"></i>
                                    <p>Pengajuan Jaminan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('approval.index') }}"
                                    class="nav-link {{ $title === 'Approval' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-check-circle"></i>
                                    <p>Approval</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('data_jaminan.index') }}"
                                    class="nav-link {{ $title === 'Data Jaminan' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-clipboard-check"></i>
                                    <p>Data Jaminan </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('data_surety_bond.index') }}"
                                    class="nav-link {{ $title === 'Data Surety Bond' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-handshake"></i>
                                    <p>Data Surety Bond</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('menejemen_user.index') }}"
                                    class="nav-link {{ $title === 'Manajemen User' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>Manajemen User</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('setting.index') }}"
                                    class="nav-link {{ $title === 'Setting' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>Setting</p>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->role->role_name == 'User HQ')
                            <li class="nav-item">
                                <a href="{{ route('dashboard.index') }}"
                                    class="nav-link {{ $title === 'Dashboard' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('laporan.index') }}"
                                    class="nav-link {{ $title === 'Laporan' ? 'active' : '' }}">
                                    <i class="nav-icon far fa-file-alt"></i>
                                    <p>Laporan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('approval.index') }}"
                                    class="nav-link {{ $title === 'Approval' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-check-circle"></i>
                                    <p>Approval</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('data_jaminan.index') }}"
                                    class="nav-link {{ $title === 'Data Jaminan' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-clipboard-check"></i>
                                    <p>Data Jaminan </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('data_surety_bond.index') }}"
                                    class="nav-link {{ $title === 'Data Surety Bond' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-handshake"></i>
                                    <p>Data Surety Bond</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('setting.index') }}"
                                    class="nav-link {{ $title === 'Setting' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>Setting</p>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->role->role_name == 'Branch User')
                            <li class="nav-item">
                                <a href="{{ route('dashboard.index') }}"
                                    class="nav-link {{ $title === 'Dashboard' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('pengajuan_jaminan.index') }}"
                                    class="nav-link {{ $title === 'Pengajuan Jaminan' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tasks"></i>
                                    <p>Pengajuan Jaminan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('data_jaminan.index') }}"
                                    class="nav-link {{ $title === 'Data Jaminan' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-clipboard-check"></i>
                                    <p>Data Jaminan </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('setting.index') }}"
                                    class="nav-link {{ $title === 'Setting' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>Setting</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </aside>

        <div class="content-wrapper">
            @yield('content')
        </div>

        <aside class="control-sidebar control-sidebar-dark">
        </aside>
    </div>

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    @yield('js')
</body>

</html>
