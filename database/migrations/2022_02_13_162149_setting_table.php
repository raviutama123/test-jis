<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mast_settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('value')->nullable();
            $table->timestamps();
            $table->softDeletes();
            // $table->tinyInteger('status_aktif')->default('1')->comment('0:Tidak Aktif; 1:Aktif;');
        });

        DB::table('mast_settings')->insert([[
            'name' => 'title',
            'value' => '',
            'created_at' => Carbon::now(),
        ], [
            'name' => 'deskripsi',
            'value' => '',
            'created_at' => Carbon::now(),
        ], [
            'name' => 'keywords',
            'value' => '',
            'created_at' => Carbon::now(),
        ], [
            'name' => 'author',
            'value' => '',
            'created_at' => Carbon::now(),
        ], [
            'name' => 'viewport',
            'value' => '',
            'created_at' => Carbon::now(),
        ],]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mast_settings');
    }
}
