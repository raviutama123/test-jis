<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('role_id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            // $table->tinyInteger('status_aktif')->default('1')->comment('0:Tidak Aktif; 1:Aktif;');
        });

        DB::table('users')->insert([[
            'name' => 'Administrator',
            'role_id' => '1',
            'email' => 'administrator@admin.com',
            'password' => bcrypt('password123'),
            'email_verified_at' => Carbon::now(),
            'remember_token' => Str::random(10),
            'created_at' => Carbon::now(),
            'created_by' => 1,
        ],]);

        DB::table('users')->insert([[
            'name' => 'User HQ',
            'role_id' => '2',
            'email' => 'userhq@admin.com',
            'password' => bcrypt('password123'),
            'email_verified_at' => Carbon::now(),
            'remember_token' => Str::random(10),
            'created_at' => Carbon::now(),
            'created_by' => 1,
        ],]);

        DB::table('users')->insert([[
        'name' => 'Branch User',
        'role_id' => '2',
        'email' => 'branchuser@admin.com',
        'password' => bcrypt('password123'),
        'email_verified_at' => Carbon::now(),
        'remember_token' => Str::random(10),
        'created_at' => Carbon::now(),
        'created_by' => 1,
        ],]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
