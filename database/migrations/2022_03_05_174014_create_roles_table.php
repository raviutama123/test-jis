<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('role_name');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            // $table->tinyInteger('status_aktif')->default('1')->comment('0:Tidak Aktif; 1:Aktif;');
        });


        DB::table('roles')->insert([[
            // 1
            'role_name' => 'Administrator',
            'created_at' => Carbon::now(),
        ], [
            // 2
            'role_name' => 'User HQ',
            'created_at' => Carbon::now(),
        ], [
            // 3
            'role_name' => 'Branch User',
            'created_at' => Carbon::now(),
        ],]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
