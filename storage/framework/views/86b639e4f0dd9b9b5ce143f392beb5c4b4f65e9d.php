

<?php $__env->startSection('content'); ?>
    <script type="text/javascript">
        var dtAktif, dtTidakAktif;
        const showLoadingSwal = () => {
            Swal.fire({
                title: 'Mohon Tunggu',
                text: "Sedang memproses data.",
                imageUrl: 'https://media.giphy.com/media/3ohBV3swUjlLSwyaTm/giphy.gif',
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false
            })
        };
    </script>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?php echo e($title); ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?php echo e($title); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header p-0 border-bottom-0">
                        <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-tenant-aktif-tab" data-toggle="pill"
                                    href="#custom-tabs-tenant-aktif" role="tab" aria-controls="custom-tabs-tenant-aktif"
                                    aria-selected="true">Tenant Aktif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-tenant-tidak-aktif-tab" data-toggle="pill"
                                    href="#custom-tabs-tenant-tidak-aktif" role="tab"
                                    aria-controls="custom-tabs-tenant-tidak-aktif" aria-selected="false">Tenant Tidak
                                    Aktif</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-four-tabContent">
                            <div class="tab-pane fade active show" id="custom-tabs-tenant-aktif" role="tabpanel"
                                aria-labelledby="custom-tabs-tenant-aktif-tab">
                                <div class="card-title">
                                    <button style="float: right; font-weight: 900;" class="btn btn-info btn-sm mb-3 mx-1"
                                        type="button" onclick="dtAktif.ajax.reload();">
                                        <i class="fa fa-recycle"></i>
                                        Segarkan Data
                                    </button>
                                    
                                </div>
                                <div class="table-responsive">
                                    <table id="dtAktif" class="table table-bordered datatable w-100">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nama Tenant</th>
                                                <th width="150" class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="custom-tabs-tenant-tidak-aktif" role="tabpanel"
                                aria-labelledby="custom-tabs-tenant-tidak-aktif-tab">
                                <div class="table-responsive">
                                    <div class="card-title">
                                        <button style="float: right; font-weight: 900;"
                                            class="btn btn-info btn-sm mb-3 mx-1" type="button"
                                            onclick="dtTidakAktif.ajax.reload();">
                                            <i class="fa fa-recycle"></i>
                                            Segarkan Data
                                        </button>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="dtTidakAktif" class="table table-bordered datatable w-100">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nama Tenant</th>
                                                    <th width="150" class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="FormModal" tabindex="-1" aria-labelledby="createModalLabel" aria-hidden="true">
    </div>

    <!-- Modal Create -->
    

    <!-- Modal Edit -->
    

    <!-- Modal Delete -->
    

    <!-- Modal Restore -->
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="/dist/css/admin_custom.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
    

    <script type="text/javascript">
        // console.log('Hi!'); 
        showLoadingSwal();
        $(document).ready(function() {
            // init datatable.
            dtAktif = $('#dtAktif').DataTable({
                dom: 'Bfrtip',
                lengthMenu: [
                    [10, 25, 50, -1],
                    ['10 rows', '25 rows', '50 rows', 'Show all']
                ],
                buttons: [
                    'pageLength', 'csv', 'pdf', 'print',
                    {
                        extend: 'excel',
                        title: 'Data Tenant - ' + Date.now(),
                        autoFilter: true,
                        //sheetName: 'Data Tenant - '+ Date.now(),
                        exportOptions: {
                            columns: [0, 1, 2],
                            modifier: {
                                page: 'all',
                                // search: 'none',   
                                // order: 'applied',
                            }
                        },
                    }
                ],
                "responsive": true,
                "autoWidth": true,
                "processing": true,
                "serverSide": true,
                "order": [
                    [1, "ASC"]
                ],
                // pageLength: 5,
                // scrollX: true,
                searchDelay: 1500,
                "order": [
                    [0, "desc"]
                ],
                ajax: '<?php echo e(route('get-datatables-tenant')); ?>',
                columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    // {data: 'id', name: 'id'},
                    {
                        data: 'tenant_name',
                        name: 'tenant_name'
                    },
                    {
                        data: 'Actions',
                        name: 'Actions',
                        orderable: false,
                        serachable: false,
                        sClass: 'text-center'
                    },
                ]
            });
            dtTidakAktif = $('#dtTidakAktif').DataTable({
                dom: 'Bfrtip',
                lengthMenu: [
                    [10, 25, 50, -1],
                    ['10 rows', '25 rows', '50 rows', 'Show all']
                ],
                buttons: [
                    'pageLength',
                ],
                "responsive": true,
                "autoWidth": true,
                "processing": true,
                "serverSide": true,
                "order": [
                    [1, "ASC"]
                ],
                // pageLength: 5,
                // scrollX: true,
                searchDelay: 1500,
                "order": [
                    [0, "desc"]
                ],
                ajax: '<?php echo e(route('get-datatables-tenant', ['dataType' => 0])); ?>',
                columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    // {data: 'id', name: 'id'},
                    {
                        data: 'tenant_name',
                        name: 'tenant_name'
                    },
                    {
                        data: 'Actions',
                        name: 'Actions',
                        orderable: false,
                        serachable: false,
                        sClass: 'text-center'
                    },
                ]
            });

            setTimeout(() => {
                Swal.close();
            }, 1 * 1000);
        });

        $(document).on('click', 'button.btn-delete', function() {
            $('#id_delete').val($(this).data('id'));
        });

        $('#formDelete').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            console.log(formData, $(this).serialize())
            $.ajax({
                type: 'POST',
                // data : $( this ).serialize(),
                data: formData,
                contentType: false,
                processData: false,
                url: $(this).attr('action'),
                beforeSend: function() {
                    $('#formDelete > button.btn-submit').addClass("disabled").html("Processing...")
                        .attr('disabled', true);
                    // $(document).find('span.error-text').text('');
                },
                complete: function() {
                    $('#formDelete > button.btn-submit').removeClass("disabled").html("Ya, hapus!")
                        .attr('disabled', false);
                },
                success: function(res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#DeleteDataModal').modal('hide');
                        $(this).trigger('reset');
                        dtAktif.ajax.reload();
                        dtTidakAktif.ajax.reload();
                        Swal.fire(
                            'Success!',
                            res.message,
                            'success'
                        )
                    }
                },
                error(err) {
                    // $.each(err.responseJSON,function(prefix,val) {
                    //     $('.'+prefix+'_error_edit').text(val[0]);
                    // })
                    console.log(err);
                    Swal.fire(
                        'Failed!',
                        err.responseJSON.message,
                        'error'
                    )
                }
            })
        });

        $(document).on('click', 'button.btn-restore', function() {
            $('#id_restore').val($(this).data('id'));
        });

        $('#formRestore').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            console.log(formData, $(this).serialize())
            $.ajax({
                type: 'POST',
                // data : $( this ).serialize(),
                data: formData,
                contentType: false,
                processData: false,
                url: $(this).attr('action'),
                beforeSend: function() {
                    $('#formRestore > button.btn-submit').addClass("disabled").html("Processing...")
                        .attr('disabled', true);
                    // $(document).find('span.error-text').text('');
                },
                complete: function() {
                    $('#formRestore > button.btn-submit').removeClass("disabled").html("Ya, pulihkan!")
                        .attr('disabled', false);
                },
                success: function(res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#RestoreDataModal').modal('hide');
                        $(this).trigger('reset');
                        dtAktif.ajax.reload();
                        dtTidakAktif.ajax.reload();
                        Swal.fire(
                            'Success!',
                            res.message,
                            'success'
                        )
                    }
                },
                error(err) {
                    // $.each(err.responseJSON,function(prefix,val) {
                    //     $('.'+prefix+'_error_edit').text(val[0]);
                    // })
                    console.log(err);
                    Swal.fire(
                        'Failed!',
                        err.responseJSON.message,
                        'error'
                    )
                }
            })
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\spero\kerjaan_ravi\aeon_cms\resources\views/pages/tenant/index.blade.php ENDPATH**/ ?>