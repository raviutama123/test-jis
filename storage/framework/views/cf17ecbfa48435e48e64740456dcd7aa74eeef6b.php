<div class="modal fade" id="RestoreDataModal" tabindex="-1" aria-labelledby="restoreModalLabel" aria-hidden="true">
    <div class="modal-dialog text-body">
        <div class="modal-content text-center">
            <div class="modal-header">
                <h5 class="modal-title" id="restoreModalLabel">Apakah Kamu Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo e(route('inventori-menu-restore')); ?>" method="post" id="formRestore">
                <div class="modal-body">
                    <h3 class="py-2">Kamu ingin memulihkan Menu ini?</h3>
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="id_restore" id="id_restore" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-success btn-submit">Ya, pulihkan!</button>
                </div>

            </form>
        </div>
    </div>
</div>
<?php /**PATH C:\application\aeon_cms\resources\views/pages/tenant/pulihModal.blade.php ENDPATH**/ ?>