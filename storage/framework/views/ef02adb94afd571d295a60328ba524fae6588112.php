<div class="modal fade" id="DeleteDataModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog text-body">
        <div class="modal-content text-center">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Apakah Kamu Yakin?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo e(route('inventori-menu-delete')); ?>" method="post" id="formDelete">
                <div class="modal-body">
                    <h3 class="py-2">Kamu ingin menghapus Menu ini?</h3>
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="id_delete" id="id_delete" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-danger btn-submit">Ya, hapus!</button>
                </div>

            </form>
        </div>
    </div>
</div>
<?php /**PATH C:\application\aeon_cms\resources\views/pages/tenant/hapusModal.blade.php ENDPATH**/ ?>