<div class="modal-dialog modal-xl" style="max-width: 85%;">
    <div class="modal-content text-body">
        <div class="modal-header">
            <h5 class="modal-title" id="createModalLabel">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="<?php echo e(route('tenant.index')); ?>/<?php echo e($items[0]->id); ?>/tambah" method="post" id="formCreate"
            enctype="multipart/form-data">
            <div class="modal-body">
                <?php echo csrf_field(); ?>
                <div class="form-group">
                    <label>Nama Menu</label>
                    <input type="text" class="form-control" placeholder="Nama Menu" id="nama_menu" name="nama_menu"
                        required autofocus>
                    <span class="nama_menu_error_create alert-danger"></span>
                </div>
                <div class="form-group">
                    <label>Foto Menu</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="foto" name="foto"
                            accept=".jpg,.jpeg,.png" onchange="loadFile(event,'outputImg')">
                        <label class="custom-file-label" for="foto">Choose file</label>
                    </div>
                    <img id="outputImg" width="300" class="img-foto" alt="Preview Foto" />
                    <br>
                    <span class="foto_error_create alert-danger"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <button type="submit" class="btn btn-success btn-submit">Simpan</button>
            </div>
        </form>
    </div>
</div>
<script>
    $('#formCreate').submit(function(e) {
        e.preventDefault();
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            url: $(this).attr('action'),
            beforeSend: function() {
                $('#formCreate > button.btn-submit').addClass("disabled").html("Processing...")
                    .attr('disabled', true);
                // $(document).find('span.error-text').text('');
            },
            complete: function() {
                $('#formCreate > button.btn-submit').removeClass("disabled").html("Simpan").attr(
                    'disabled', false);
            },
            success: function(res) {
                console.log(res);
                if (res.success == true) {
                    $('#FormModal').modal('hide');
                    $(this).trigger('reset');
                    dtAktif.ajax.reload();
                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                }
            },
            error(err) {
                $.each(err.responseJSON, function(prefix, val) {
                    // console.log(prefix,val)
                    $('.' + prefix + '_error_create').text(val[0]);
                })
                console.log(err)

                Swal.fire(
                    'Failed!',
                    err.responseJSON.message,
                    'error'
                )

                console.log(err);
            }
        })
    });
</script>
<?php /**PATH C:\application\aeon_cms\resources\views/pages/tenant/tambahModal.blade.php ENDPATH**/ ?>