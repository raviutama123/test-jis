<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    use HasFactory;
    // , SoftDeletes;
    protected $table = "m_tenant";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = [];

    protected static function booted(): void
    {
        static::creating(function (self $model) {
            $model->updated_at = null;
        });
    }

    public function menu()
    {
        return $this->hasMany(Tenant::class, 'id_menu', 'id');
    }

    public function scopeAktif($query)
    {
        // return $query->where('status_aktif', 1);
        return $query->whereNull('deleted_at');
    }

    public function scopeTidakAktif($query)
    {
        // return $query->where('status_aktif', 0);
        return $query->whereNotNull('deleted_at');
    }
}
