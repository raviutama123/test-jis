<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
use HasFactory;
// , SoftDeletes;
protected $table = "roles";
protected $primaryKey = 'id';
protected $guarded = ['id'];
protected $fillable = [];

public function scopeAktif($query)
{
return $query->where('status_aktif', 1);
}

public function scopeTidakAktif($query)
{
return $query->where('status_aktif', 0);
}

public function isAdmin()
{
return $this->hasRole->name('Admin');
}

}
