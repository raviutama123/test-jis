<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    // , SoftDeletes;
    protected $table = "m_menu";
    protected $primaryKey = 'id_menu';
    protected $guarded = ['id_menu'];
    protected $fillable = [];

    protected static function booted(): void
    {
        static::creating(function (self $model) {
            $model->updated_at = null;
        });
    }

    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'id');
    }

    public function scopeAktif($query)
    {
        return $query->where('status_aktif', 1);
        // return $query->whereNull('deleted_at');
    }

    public function scopeTidakAktif($query)
    {
        return $query->where('status_aktif', 0);
        // return $query->whereNotNull('deleted_at');
    }
}
