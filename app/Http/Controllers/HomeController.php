<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $data = User::select('role_id','email','name')->get();
        // // dd($data);
        // return view('home', [
        //     'title' => 'Home',
        //     'agen' => $data->where('role_id',1)->count(),
        //     'jamaah' => $data->where('role_id',2)->count(),
        //     'cabang' => $data->where('role_id',3)->count(),
        // ]);

        return view('home', [
            'title' => 'Home',
        ]);
    }
}
